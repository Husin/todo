package com.brontoproject.todolist_amdroid;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class AboutFragment extends Fragment {
    public static MainActivity mainActivity;
    public static AboutFragment newInstance(MainActivity activity){
        mainActivity = activity;
        return new AboutFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = LayoutInflater.from(mainActivity).inflate(R.layout.fragment_about, container, false);
        return view;
    }
}
