package com.brontoproject.todolist_amdroid;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    DbHelper dbHelper;
    public ArrayAdapter<String> adapterList, adapterDT;//penerima data dari db
    public ListView listTodo, listDT;//menampilkan data ke layar
    private int mYear, mMonth, mDay, mHour, mMinute;
    ImageButton buttonMain, buttonProfile, buttonAbout;

    //create task activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //transaction db
        dbHelper = new DbHelper(this);
        listTodo = (ListView)findViewById(R.id.list_todo);
        listDT = (ListView)findViewById(R.id.list_dt);
        loadDateTime();
         loadTodoList();
         buttonMain = (ImageButton)findViewById(R.id.btnFmain);
         buttonProfile = (ImageButton)findViewById(R.id.btnFprofile);
         buttonAbout = (ImageButton)findViewById(R.id.btnFabout);
         //fragment transaction

        buttonMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
                overridePendingTransition(0,0);

//                changeContent(MainFragment.newInstance(MainActivity.this));
            }
        });
        buttonProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(ProfileFragment.newInstance(MainActivity.this));
            }
        });
        buttonAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(AboutFragment.newInstance(MainActivity.this));
            }
        });


    }
    private void loadTodoList() {
        ArrayList<String> todoList = dbHelper.showTask();

        //show task
        if (adapterList == null){
            adapterList = new ArrayAdapter<String>(this, R.layout.row_task, R.id.judulTodo,todoList);
            listTodo.setAdapter(adapterList);
            Log.d("isi List", String.valueOf(todoList));
        }else {
            adapterList.clear();
            adapterList.addAll(todoList);
            adapterList.notifyDataSetChanged();
        }
    }
    private void loadDateTime(){
        ArrayList<String> todoDT = dbHelper.showDateTime();

        //show task
        if (adapterDT == null){
            adapterDT = new ArrayAdapter<String>(this, R.layout.row_task, R.id.date_time_todo,todoDT);
            listDT.setAdapter(adapterDT);
            Log.d("isi DT", String.valueOf(todoDT));
        }else {
            adapterDT.clear();
            adapterDT.addAll(todoDT);
            adapterDT.notifyDataSetChanged();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);

        Drawable icon = menu.getItem(0).getIcon();
        icon.mutate();
        icon.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_IN);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.addTodoTask:
                LayoutInflater factory = LayoutInflater.from(this);
                final View textEntryView = factory.inflate(R.layout.text_entry, null);
                final EditText inputUser = (EditText) textEntryView.findViewById(R.id.et_input_user);
                final TextView inputDate = (TextView) textEntryView.findViewById(R.id.tx_date);
                final TextView inputTime = (TextView) textEntryView.findViewById(R.id.tx_time);
                final Button buttonDate = (Button) textEntryView.findViewById(R.id.btn_set_date);
                final Button buttonTime = (Button) textEntryView.findViewById(R.id.btn_set_time);
                buttonDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pickDate(inputDate);
                    }
                });
                buttonTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pickTime(inputTime);
                    }
                });
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setIcon(R.drawable.logo).setTitle("Add Todo")
                        .setView(textEntryView)
                        .setPositiveButton("Tambah",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                String dateValue = String.valueOf(inputDate.getText());
                                String timeValue = String.valueOf(inputTime.getText());
                                addList(inputUser, dateValue, timeValue);
//                                pickTxDate.setText(String.valueOf(inputDate.getText()));
//                                pickTxTime.setText(String.valueOf(inputTime.getText()));
                                Log.d("Isi date",String.valueOf(dateValue));
                                Log.d("Isi time", String.valueOf(timeValue));
                                loadTodoList();
                                loadDateTime();
                            }
                        }).setNegativeButton("Batal", null);
                alert.show();
        }
        return super.onOptionsItemSelected(item);

    }
//yang salah array listnya, crud udah bener, kesalahannya textview terduplikat, bukan membuat objek baru,
// jadi harus make
// View parent = (View) view.getParent();
// TextView todoTextView = (TextView) parent.findViewById(R.id.judulTodo);
    public void updateTodo(View view){
        View parent = (View) view.getParent();
        TextView todoTextView = (TextView) parent.findViewById(R.id.judulTodo);
        TextView todoDTView = (TextView) parent.findViewById(R.id.date_time_todo);
        final String beforeTask = String.valueOf(todoTextView.getText());
        final String beforeDateTime = String.valueOf(todoDTView.getText());

        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.text_entry, null);
        final EditText inputUser = (EditText) textEntryView.findViewById(R.id.et_input_user);
        final TextView inputDate = (TextView) textEntryView.findViewById(R.id.tx_date);
        final TextView inputTime = (TextView) textEntryView.findViewById(R.id.tx_time);
        final Button buttonDate = (Button) textEntryView.findViewById(R.id.btn_set_date);
        final Button buttonTime = (Button) textEntryView.findViewById(R.id.btn_set_time);
        inputUser.setText(beforeTask);
//        inputDate.setText(beforeDateTime);
//        inputTime.setText(beforeDateTime);
//        inputDate.setText(beforeDateTime);
        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDate(inputDate);
            }
        });
        buttonTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickTime(inputTime);
            }
        });
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setIcon(R.drawable.logo).setTitle("Update Todo")
                .setView(textEntryView)
                .setPositiveButton("Perbarui",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                String dateValue = String.valueOf(inputDate.getText());
                                String timeValue = String.valueOf(inputTime.getText());
                                updateList(beforeTask, inputUser, dateValue, timeValue);
                                Log.d("Isi date",String.valueOf(inputDate.getText()));
                                Log.d("Isi time", String.valueOf(inputTime.getText()));
                                loadTodoList();
                                loadDateTime();
                            }
                        }).setNegativeButton("Batal",null);
        alert.show();
    }
    public void deleteTodo(View view){
        View parent = (View) view.getParent();
        TextView todoTextView = (TextView) parent.findViewById(R.id.judulTodo);
        String task = String.valueOf(todoTextView.getText());
        Log.d("isi data delete", task);
        dbHelper.deltTodo(task);
        loadTodoList();
        loadDateTime();
    }
    public void pickDate(final TextView txtDate){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    public void pickTime(final TextView txtTime){
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        txtTime.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }
    public void addList(EditText input, String dateValue, String timeValue){
        String task =String.valueOf(input.getText());
                        dbHelper.addTodo(task, dateValue,timeValue);
                    Log.d("isi add",task);
                        loadTodoList();
    }
    public void updateList(String oldInput,EditText newInput,String dateValue, String timeValue){
        String newTask =String.valueOf(newInput.getText());
                        Log.d("isi update",newTask);
                        dbHelper.upTodo(oldInput,newTask, dateValue, timeValue);
                        loadTodoList();
    }
    public void changeContent(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, fragment).commitAllowingStateLoss();
    }
}
