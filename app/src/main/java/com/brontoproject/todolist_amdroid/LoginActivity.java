package com.brontoproject.todolist_amdroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brontoproject.todolist_amdroid.MainActivity;
import com.brontoproject.todolist_amdroid.R;

public class LoginActivity extends AppCompatActivity {

    EditText editTextUserName, editTextPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aktivity_login);

        editTextUserName = findViewById(R.id.username);
        editTextPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = editTextUserName.getText().toString();
                String password = editTextPassword.getText().toString();

                if (userName.trim().equalsIgnoreCase("")){
                    editTextUserName.setError("Username tidak boleh kosong");
                    editTextUserName.requestFocus();
                }
                else if(password.trim().equalsIgnoreCase("")){
                    editTextPassword.setError("Password tidak boleh kosong");
                    editTextPassword.requestFocus();
                }
                else{
                    if (userName.equalsIgnoreCase("amdroid")&& password.equalsIgnoreCase("12345")){
                       Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                       startActivity(intent);
                       finish();
                    }else{
                        Toast.makeText(LoginActivity.this, "Username dan Password tidak sesuai", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

}