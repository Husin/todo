package com.brontoproject.todolist_amdroid;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    public static MainActivity mainActivity;
    public static MainFragment newInstance(MainActivity activity){
        mainActivity = activity;
        return new MainFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = LayoutInflater.from(mainActivity).inflate(R.layout.activity_main, container, false);
        return view;
    }
}
