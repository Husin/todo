package com.brontoproject.todolist_amdroid;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class ProfileFragment extends Fragment {

    public static MainActivity mainActivity;
    public static ProfileFragment newInstance(MainActivity activity){
        mainActivity = activity;
        return new ProfileFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = LayoutInflater.from(mainActivity).inflate(R.layout.fragment_profile, container, false);
        return view;
    }
}
